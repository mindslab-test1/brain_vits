import argparse
import os, sys
import torch
from scipy.io.wavfile import write

import commons
import utils
from models import SynthesizerTrn
from text.symbols import symbol_len
from text import text_to_sequence
origin_path = os.getcwd()
os.chdir('./brain_rbkog2p')
sys.path.append('.')
import brain_rbkog2p.process as process
os.chdir(origin_path)

def main(args):
    def get_text(text, hps):
        text_norm, tone = text_to_sequence(text, hps.data.languages, hps.data.text_cleaners)
        if hps.data.add_blank:
            text_norm = commons.intersperse(text_norm, 0)
            tone = commons.intersperse(tone, 0)
        text_norm = torch.LongTensor(text_norm)
        tone = torch.LongTensor(tone)
        return text_norm, tone
        
    dict = process.load_dict("./brain_rbkog2p/word_dict.txt")
    for_dict = process.load_dict('./brain_rbkog2p/foreign_dict.txt')

    hps = utils.get_hparams_from_file(args.config)

    net_g = SynthesizerTrn(
        symbol_len(hps.data.languages),
        hps.data.filter_length // 2 + 1,
        hps.train.segment_size // hps.data.hop_length,
        n_speakers=len(hps.data.speakers),
        **hps.model
    ).cuda()

    _ = net_g.eval()
    _ = utils.load_checkpoint(args.checkpoint_path, model_g=net_g)

    text = args.text
    os.chdir('./brain_rbkog2p')
    phone = process.phoneme_process(dict, for_dict, text)
    os.chdir(origin_path)
    duration_control = args.duration_control
    phone_norm, tone = get_text(phone, hps)
    with torch.no_grad():
        x_tst = phone_norm.cuda().unsqueeze(0)
        t_tst = tone.cuda().unsqueeze(0)
        x_tst_lengths = torch.LongTensor([phone_norm.size(0)]).cuda()
        speaker_id = torch.LongTensor([args.speaker_id]).cuda()
        audio = net_g.infer(x_tst, t_tst, x_tst_lengths, sid=speaker_id, noise_scale=.667, noise_scale_w=0.8, length_scale=duration_control)[0][0,0].data.cpu().float().numpy()
    os.makedirs(args.wav_path, exist_ok=True)
    write(
        os.path.join(args.wav_path, str(args.speaker_id) + "_" + str(duration_control) + "_" + text[:30] + ".wav"),
        hps.data.sampling_rate,
        audio,
    )

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-t', 
        '--text', 
        type=str,
        required=True,
        help='Text to synthesize'
    )
    parser.add_argument(
        '-s', 
        '--speaker_id', 
        type=int,
        default=0,
        help='Speaker ID'
    )
    parser.add_argument(
        '-c', 
        '--config', 
        type=str, 
        default="./configs/default.yaml",
        help='Path to configuration file'
    )
    parser.add_argument(
        '-p', 
        '--checkpoint_path', 
        type=str, 
        required=True,
        help='Path to checkpoint file'
    )
    parser.add_argument(
        '-w', 
        '--wav_path', 
        default="./wavs",
        help='Path to save wav file'
    )
    parser.add_argument(
        '-d', 
        '--duration_control', 
        type=float,
        default=1.0,
        help='Duration control'
    )

    args = parser.parse_args()
    main(args)
