# VITS

Kakao의 [**Conditional Variational Autoencoder with Adversarial Learning for End-to-End Text-to-Speech**](https://arxiv.org/abs/2106.06103) 구현 코드입니다.   
Official open source인 [**jaywalnut310's implementation**](https://github.com/jaywalnut310/vits)을 기반으로 하였습니다.   
학습 및 서버 띄우기 메뉴얼은 [confluence](https://pms.maum.ai/confluence/pages/viewpage.action?pageId=43094955)에 있습니다. 아래는 간단한 요약이니 자세한 내용을 보려면 confluence를 참고해주세요.

P.S. 영문 TTS 데모는 [demo](https://jaywalnut310.github.io/vits-demo/index.html)에서 확인 가능합니다.

## Start Learning

### Docker
Dockerfile을 build하거나 pull한 다음 컨테이너를 만들고 실행해주세요.   
Configuration yaml file과 metadata는 양식에 맞춰 작성한 뒤 사용해주세요.
- `wav_path|text|speaker` 형태로 된 metadata를 사용하고, 데이터 관련 설정을 config에 적어두세요.

### Pre-requisites
```sh
# Cython-version Monotonoic Alignment Search
cd monotonic_align
python3 setup.py build_ext --inplace
```
Dockerfile에 해당 명령이 포함되어 있으므로, 기본적으로 필요는 없으나 Monotonic Alignment Search가 없다는 에러 뜨면 위의 명령어를 입력하세요.

### Baseline Learning
```sh
python3 train.py -c CONFIG_PATH -m MODEL_NAME
```
체크포인트 써서 이어 학습하는 경우 -r 옵션 뒤에 이어서 학습할 checkpoint 경로를 입력하면 저장된 step에서부터 로드해 학습합니다.

### Fine-tuning
```sh
python3 train.py -c CONFIG_PATH -m TUNING_MODEL_NAME -t BASELINE_CHECKPOINT_PATH
```
TUNING_MODEL_NAME을 새로 정하고 -t 옵션 뒤에 baseline model의 checkpoint 경로를 붙이면 fine-tuning하여 학습합니다.

### Inference
```sh
python3 inference.py -t TEXT -s SPEAKER_ID -c CONFIG_PATH -p CHECKPOINT_PATH -w WAVEFILE_PATH
```
Inference 과정을 통해 TEXT에 해당되는 음성을 만들고 WAVEFILE_PATH에 저장합니다.

명령어에 -d 옵션을 붙이면 duration control이 가능합니다.

이때 필요한 g2p 기능 때문에 brain_rbkog2p를 서브모듈로 넣어둔 상태입니다. 따라서 brain_rbkog2p의 requirement가 설치되어 있어야 합니다.

Interactive하게 다루고 싶다면 `inference.ipynb`를 사용하시면 됩니다.

## Others

### Warning
학습을 진행하면서 semaphore 관련 경고가 뜨는 경우가 발생할 수 있습니다.

별 문제는 없지만 화면이 더러워지거나 progress bar가 잘 안 보일 수 있으니 해당 경고를 무시하려면 python 명령어 입력하면서 옵션으로 -W ignore를 붙이고, 훈련 시에는 아래 예시처럼 -w 옵션(--ignore_warning)을 덧붙이면 됩니다.
```sh
CUDA_VISIBLE_DEVICES=0,1 python3 -W ignore train.py -c CONFIG_PATH -m MODEL_NAME --ignore_warning
```

### TensorBoard
```sh
tensorboard --port=6006 --logdir LOG_PATH --bind_all
# Or
tensorboard --port=6006 --logdir_spec NAME1:LOG_PATH1,NAME2:LOG_PATH2,NAME3:LOG_PATH3 --bind_all
```
명령어에 --bind_all을 붙여야 접근 가능합니다.