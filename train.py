import os
import argparse
import torch
from torch.nn import functional as F
from torch.utils.data import DataLoader
from torch.utils.tensorboard import SummaryWriter
import torch.multiprocessing as mp
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.cuda.amp import autocast, GradScaler
from tqdm import tqdm

import commons
import utils
from data_utils import (
    TextAudioSpeakerLoader,
    TextAudioSpeakerCollate,
    DistributedBucketSampler,
    create_spec
)
from models import (
    SynthesizerTrn,
    MultiPeriodDiscriminator,
)
from losses import (
    generator_loss,
    discriminator_loss,
    feature_loss,
    kl_loss
)
from mel_processing import mel_spectrogram_torch, spec_to_mel_torch
from text.symbols import symbol_len


torch.backends.cudnn.benchmark = True
global_step = 0


def main(args):
    """Assume Single Node Multi GPUs Training Only"""
    assert torch.cuda.is_available(), "CPU training is not allowed."

    n_gpus = torch.cuda.device_count()
    os.environ['MASTER_ADDR'] = 'localhost'
    os.environ['MASTER_PORT'] = '80000'

    hps = utils.get_hparams(args)
    # create spectrogram files
    create_spec(hps.data.training_files, hps.data)
    create_spec(hps.data.validation_files, hps.data)
    mp.spawn(run, nprocs=n_gpus, args=(n_gpus, hps, args))


def count_parameters(model, scale=1000000): 
    return sum(p.numel() for p in model.parameters() if p.requires_grad) / scale


def run(rank, n_gpus, hps, args):
    global global_step
    if rank == 0:
        logger = utils.get_logger(hps.model_dir)
        logger.info('MODEL NAME: {} in {}'.format(args.model, hps.model_dir))
        logger.info('GPU: Use {} gpu(s) with batch size {} (FP16 running: {})'.format(n_gpus, hps.train.batch_size, hps.train.fp16_run))

        utils.check_git_hash(hps.model_dir)
        writer = SummaryWriter(log_dir=hps.model_dir)

    dist.init_process_group(
        backend='nccl', init_method='env://', world_size=n_gpus, rank=rank
    )
    torch.manual_seed(hps.train.seed)
    torch.cuda.set_device(rank)

    use_persistent_workers = hps.data.persistent_workers
    use_pin_memory = not use_persistent_workers
    train_dataset = TextAudioSpeakerLoader(hps.data.training_files, hps.data)
    train_sampler = DistributedBucketSampler(
        train_dataset,
        hps.train.batch_size,
        [32, 300, 400, 500, 600, 700, 800, 900, 1000],
        num_replicas=n_gpus,
        rank=rank,
        shuffle=True
    )
    collate_fn = TextAudioSpeakerCollate()
    train_loader = DataLoader(
        train_dataset, 
        num_workers=8, 
        shuffle=False, 
        pin_memory=use_pin_memory,
        collate_fn=collate_fn, 
        persistent_workers=use_persistent_workers, 
        batch_sampler=train_sampler
    )
    if rank == 0:
        eval_dataset = TextAudioSpeakerLoader(hps.data.validation_files, hps.data)
        eval_loader = DataLoader(
            eval_dataset, 
            num_workers=8, 
            shuffle=False,
            batch_size=hps.train.batch_size, 
            pin_memory=use_pin_memory,
            drop_last=False, 
            collate_fn=collate_fn,
            persistent_workers=use_persistent_workers, 
        )

    net_g = SynthesizerTrn(
        symbol_len(hps.data.languages),
        hps.data.filter_length // 2 + 1,
        hps.train.segment_size // hps.data.hop_length,
        n_speakers=len(hps.data.speakers),
        **hps.model
    ).cuda(rank)
    net_d = MultiPeriodDiscriminator(hps.model.use_spectral_norm).cuda(rank)

    if rank == 0:
        logger.info(
            'MODEL SIZE: G {:.2f}M and D {:.2f}M'.format(
                count_parameters(net_g), 
                count_parameters(net_d),
            )
        )

    optim_g = torch.optim.AdamW(
        net_g.parameters(),
        hps.train.learning_rate,
        betas=hps.train.betas,
        eps=hps.train.eps
    )
    optim_d = torch.optim.AdamW(
        net_d.parameters(),
        hps.train.learning_rate,
        betas=hps.train.betas,
        eps=hps.train.eps
    )
    net_g = DDP(net_g, device_ids=[rank], find_unused_parameters = True)
    net_d = DDP(net_d, device_ids=[rank], find_unused_parameters = True)

    if args.transfer:
        _, _, _, _, _, _, _ = utils.load_checkpoint(args.transfer, rank, net_g, net_d, None, None)
        epoch_str = 1
        global_step = 0
    elif args.resume:
        _, _, _, _, _, epoch_save, _ = utils.load_checkpoint(args.resume, rank, net_g, net_d, optim_g, optim_d)
        epoch_str = epoch_save + 1
        global_step = epoch_save * len(train_loader) + 1
    else:
        epoch_str = 1
        global_step = 0

    scheduler_g = torch.optim.lr_scheduler.ExponentialLR(
        optim_g, gamma=hps.train.lr_decay, last_epoch=epoch_str - 2
    )
    scheduler_d = torch.optim.lr_scheduler.ExponentialLR(
        optim_d, gamma=hps.train.lr_decay, last_epoch=epoch_str - 2
    )

    scaler = GradScaler(enabled=hps.train.fp16_run)
    if rank == 0:
        outer_bar = tqdm(total=hps.train.epochs, desc="Training", position=0, leave=False)
        outer_bar.update(epoch_str)

    for epoch in range(epoch_str, hps.train.epochs + 1):
        if rank == 0:
            train_and_evaluate(
                rank, 
                epoch, 
                hps, 
                [net_g, net_d], 
                [optim_g, optim_d], 
                [scheduler_g, scheduler_d], 
                scaler, 
                [train_loader, eval_loader], 
                writer
            )
        else:
            train_and_evaluate(
                rank, 
                epoch, 
                hps, 
                [net_g, net_d], 
                [optim_g, optim_d], 
                [scheduler_g, scheduler_d], 
                scaler, 
                [train_loader, None], 
                None
            )
        scheduler_g.step()
        scheduler_d.step()
        if rank == 0:
            outer_bar.update(1)


def train_and_evaluate(rank, epoch, hps, nets, optims, schedulers, scaler, loaders, writer):
    net_g, net_d = nets
    optim_g, optim_d = optims
    scheduler_g, scheduler_d = schedulers
    train_loader, eval_loader = loaders

    train_loader.batch_sampler.set_epoch(epoch)
    global global_step

    net_g.train()
    net_d.train()
    if rank == 0:
        inner_bar = tqdm(total=len(train_loader), desc="Epoch {}".format(epoch), position=1, leave=False)

    for batch_idx, (x, x_lengths, spec, spec_lengths, y, y_lengths, speakers, tone) in enumerate(train_loader):
        x, x_lengths = x.cuda(rank, non_blocking=True), x_lengths.cuda(
            rank, non_blocking=True
        )
        spec, spec_lengths = spec.cuda(
            rank, non_blocking=True), spec_lengths.cuda(rank, non_blocking=True)
        y, y_lengths = y.cuda(rank, non_blocking=True), y_lengths.cuda(
            rank, non_blocking=True)
        speakers = speakers.cuda(rank, non_blocking=True)
        tone = tone.cuda(rank, non_blocking=True)

        with autocast(enabled=hps.train.fp16_run):
            y_hat, l_length, attn, ids_slice, x_mask, z_mask,\
                (z, z_p, m_p, logs_p, m_q, logs_q) = net_g(
                    x, tone, x_lengths, spec, spec_lengths, speakers
                )

            mel = spec_to_mel_torch(
                spec,
                hps.data.filter_length,
                hps.data.n_mel_channels,
                hps.data.sampling_rate,
                hps.data.mel_fmin,
                hps.data.mel_fmax
            )
            y_mel = commons.slice_segments(
                mel, ids_slice, hps.train.segment_size // hps.data.hop_length
            )
            y_hat_mel = mel_spectrogram_torch(
                y_hat.squeeze(1),
                hps.data.filter_length,
                hps.data.n_mel_channels,
                hps.data.sampling_rate,
                hps.data.hop_length,
                hps.data.win_length,
                hps.data.mel_fmin,
                hps.data.mel_fmax
            )

            y = commons.slice_segments(
                y, ids_slice * hps.data.hop_length, hps.train.segment_size
            )  # sliced

            # Discriminator
            y_d_hat_r, y_d_hat_g, _, _ = net_d(y, y_hat.detach())
            with autocast(enabled=False):
                loss_disc, losses_disc_r, losses_disc_g = discriminator_loss(
                    y_d_hat_r, y_d_hat_g
                )
                loss_disc_all = loss_disc

        optim_d.zero_grad()
        scaler.scale(loss_disc_all).backward()
        scaler.unscale_(optim_d)
        grad_norm_d = commons.clip_grad_value_(net_d.parameters(), None)
        scaler.step(optim_d)

        with autocast(enabled=hps.train.fp16_run):
            # Generator
            y_d_hat_r, y_d_hat_g, fmap_r, fmap_g = net_d(y, y_hat)
            with autocast(enabled=False):
                loss_dur = torch.sum(l_length.float())
                loss_mel = F.l1_loss(y_mel, y_hat_mel) * hps.train.c_mel
                loss_kl = kl_loss(
                    z_p, logs_q, m_p, logs_p, z_mask
                ) * hps.train.c_kl

                loss_fm = feature_loss(fmap_r, fmap_g)
                loss_gen, losses_gen = generator_loss(y_d_hat_g)
                loss_gen_all = loss_gen + loss_fm + loss_mel + loss_dur + loss_kl
        optim_g.zero_grad()
        scaler.scale(loss_gen_all).backward()
        scaler.unscale_(optim_g)
        grad_norm_g = commons.clip_grad_value_(net_g.parameters(), None)
        scaler.step(optim_g)
        scaler.update()

        if rank == 0:
            inner_bar.update(1)
            inner_bar.set_description("Epoch {} | g {: .04f} d {: .04f}|".format(epoch, loss_gen_all, loss_disc_all))
            if global_step % hps.train.log_interval == 0:
                lr = optim_g.param_groups[0]['lr']

                scalar_dict = {
                    "learning_rate": lr,
                    "loss/g/score": sum(losses_gen), 
                    "loss/g/fm": loss_fm, 
                    "loss/g/mel": loss_mel,
                    "loss/g/dur": loss_dur,
                    "loss/g/kl": loss_kl,
                    "loss/g/total": loss_gen_all,
                    "loss/d/real": sum(losses_disc_r), 
                    "loss/d/gen": sum(losses_disc_g), 
                    "loss/d/total": loss_disc_all,
                }

                utils.summarize(
                    writer=writer,
                    global_step=global_step,
                    scalars=scalar_dict
                )
            if global_step % hps.train.eval_interval == 0:
                evaluate(hps, global_step, epoch, net_g, eval_loader, writer)

        global_step += 1
        
    if rank == 0:
        if epoch % hps.train.save_interval == 0:
            utils.save_checkpoint(
                net_g, 
                optim_g, 
                net_d, 
                optim_d, 
                hps, 
                epoch, 
                hps.train.learning_rate, 
                os.path.join(hps.model_dir, "{}_{}.pth".format(hps.model_name, epoch))
            )


def evaluate(hps, current_step, epoch, generator, eval_loader, writer):
    generator.eval()
    n_sample = hps.train.n_sample
    with torch.no_grad():
        loss_val = 0
        val_bar = tqdm(total=len(eval_loader), desc="Validation (Step {})".format(current_step), position=1, leave=False)
        for batch_idx, (x, x_lengths, spec, spec_lengths, y, y_lengths, speakers, tone) in enumerate(eval_loader):
            x, x_lengths = x.cuda(0, non_blocking=True), x_lengths.cuda(0, non_blocking=True)
            spec, spec_lengths = spec.cuda(0, non_blocking=True), spec_lengths.cuda(0, non_blocking=True)
            y, y_lengths = y.cuda(0, non_blocking=True), y_lengths.cuda(0, non_blocking=True)
            speakers = speakers.cuda(0, non_blocking=True)
            tone = tone.cuda(0, non_blocking=True)

            with autocast(enabled=hps.train.fp16_run):
                y_hat, l_length, attn, ids_slice, x_mask, z_mask,\
                    (z, z_p, m_p, logs_p, m_q, logs_q) = generator.module(
                        x, tone, x_lengths, spec, spec_lengths, speakers
                    )

                mel = spec_to_mel_torch(
                    spec,
                    hps.data.filter_length,
                    hps.data.n_mel_channels,
                    hps.data.sampling_rate,
                    hps.data.mel_fmin,
                    hps.data.mel_fmax
                )
                y_mel = commons.slice_segments(
                    mel, ids_slice, hps.train.segment_size // hps.data.hop_length
                )
                y_hat_mel = mel_spectrogram_torch(
                    y_hat.squeeze(1),
                    hps.data.filter_length,
                    hps.data.n_mel_channels,
                    hps.data.sampling_rate,
                    hps.data.hop_length,
                    hps.data.win_length,
                    hps.data.mel_fmin,
                    hps.data.mel_fmax
                )
                with autocast(enabled=False):
                    loss_mel = F.l1_loss(y_mel, y_hat_mel) * hps.train.c_mel
                    loss_val += loss_mel.item()

            if batch_idx == 0:
                x = x[:n_sample]
                x_lengths = x_lengths[:n_sample]
                spec = spec[:n_sample]
                spec_lengths = spec_lengths[:n_sample]
                y = y[:n_sample]
                y_lengths = y_lengths[:n_sample]
                speakers = speakers[:n_sample]
                tone = tone[:1]
            
                y_hat, _, mask, * \
                    _ = generator.module.infer(x, tone, x_lengths, speakers, max_len=2000)
                y_hat_mel_length = mask.sum([1, 2]).long()
                y_hat_lengths = y_hat_mel_length * hps.data.hop_length

                mel = spec_to_mel_torch(
                    spec,
                    hps.data.filter_length,
                    hps.data.n_mel_channels,
                    hps.data.sampling_rate,
                    hps.data.mel_fmin,
                    hps.data.mel_fmax
                )
                y_hat_mel = mel_spectrogram_torch(
                    y_hat.squeeze(1).float(),
                    hps.data.filter_length,
                    hps.data.n_mel_channels,
                    hps.data.sampling_rate,
                    hps.data.hop_length,
                    hps.data.win_length,
                    hps.data.mel_fmin,
                    hps.data.mel_fmax
                )

                if y_hat_mel.size(2) < mel.size(2):
                    zero = torch.full((n_sample, y_hat_mel.size(1), mel.size(2) - y_hat_mel.size(2)), -11.5129).to(y_hat_mel.device)
                    y_hat_mel = torch.cat((y_hat_mel, zero), dim=2)
                    
                    ids = torch.arange(0, mel.size(2)).unsqueeze(0).expand(mel.size(1), -1).unsqueeze(0).expand(n_sample, -1, -1).to(y_hat_mel_length.device)
                    mask = ids > y_hat_mel_length.unsqueeze(1).expand(-1, mel.size(1)).unsqueeze(2).expand(-1, -1, mel.size(2))
                    y_hat_mel.masked_fill_(mask, -11.5129)

                image_dict = dict()
                audio_dict = dict()
                for i in range(n_sample):
                    image_dict.update(
                        {"gen/mel_{}".format(i): utils.plot_spectrogram_to_numpy(y_hat_mel[i].cpu().numpy())}
                    )
                    audio_dict.update(
                        {"gen/audio_{}".format(i): y_hat[i, :, :y_hat_lengths[i]]}
                    )

                if current_step == 0:
                    for i in range(n_sample):
                        image_dict.update(
                            {"gt/mel_{}".format(i): utils.plot_spectrogram_to_numpy(mel[i].cpu().numpy())}
                        )
                        audio_dict.update(
                            {"gt/audio_{}".format(i): y[i, :, :y_lengths[i]]}
                        )

                utils.summarize(
                    writer=writer,
                    global_step=epoch,
                    images=image_dict,
                    audios=audio_dict,
                    audio_sampling_rate=hps.data.sampling_rate
                )
            val_bar.update(1)
        loss_val = loss_val / len(eval_loader)
        utils.summarize(
            writer=writer,
            global_step=current_step,
            scalars={"loss/val/mel": loss_val}
        )
    generator.train()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-c', 
        '--config', 
        type=str, 
        default="./configs/default.yaml",
        help='Path to configuration file'
    )
    parser.add_argument(
        '-m', 
        '--model', 
        type=str, 
        required=True,
        help='Model name'
    )
    parser.add_argument(
        '-r', 
        '--resume', 
        type=str,
        help='Path to checkpoint for resume'
    )
    parser.add_argument(
        '-t', 
        '--transfer', 
        type=str,
        help='Path to baseline checkpoint for transfer'
    )
    parser.add_argument(
        '-w', 
        '--ignore_warning', 
        action="store_true",
        help='Ignore warning message'
    )

    args = parser.parse_args()
    if args.ignore_warning:
        import warnings
        warnings.filterwarnings(action='ignore')

    main(args)